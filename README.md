# `LoadingCache<K, V>` manager
This is a proof-of-concept demonstrating the addition of an injectable `LoadingCacheManager`.

The `LoadingCacheManager` is able to build, register and retrieve instance of [Caffeine][caffeine]'s [`LoadingCache<K, V>`][loading-cache]. It functions in much the same way as Spring's existing [CacheManager][cache-manager].

## Usage
Obtain a `LoadingCache<K,V>` builder instance:
```java
loadingCacheManager.<K, V>newBuilder();
```

#### Builder methods
| Builder method name                     | Purpose                                              | Required |
|-----------------------------------------|------------------------------------------------------|----------|
| `.name(String)`                         | Cache name (for retrieval via `LoadingCacheManager`) | Yes      |
| `.spec(Caffeine<Object, Object>)`       | Cache spec                                           | No       |
| `.spec(String)`                         | Cache spec (parsed via `Caffeine.from`)              | No       |
| `.loadFunction(LoadFunction<K, V>)`     | Load function (key -> new value)                     | Yes      |
| `.reloadFunction(ReloadFunction<K, V>)` | Reload function (key + old value -> new value)       | No       |
| `.buildAndRegister()`                   | Build and register a new `LoadingCache<K, V>`        | N/A      |

- Caffeine specs are looked up in classpath resource `caffeine.properties` by default. Example contents:
   ```properties
   caffeine.spec.myCache=maximumSize=10
   ```
- Reload functions default to returning the old value if any `Exception` is thrown from the `loadFunction`.

#### Retrieving a cache
```java
loadingCache<K, V> cache = loadingCacheManager.getCache("myCache");
```

#### Getting all cache names
```java
Collection<String> cacheNames = loadingCacheManager.getCacheNames();
```

[caffeine]: https://github.com/ben-manes/caffeine
[loading-cache]: https://javadoc.io/doc/com.github.ben-manes.caffeine/caffeine/latest/com/github/benmanes/caffeine/cache/LoadingCache.html
[cache-manager]: https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/cache/CacheManager.html