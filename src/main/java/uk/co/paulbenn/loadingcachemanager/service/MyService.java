package uk.co.paulbenn.loadingcachemanager.service;

import com.github.benmanes.caffeine.cache.LoadingCache;
import org.springframework.stereotype.Service;
import uk.co.paulbenn.loadingcachemanager.cache.LoadingCacheManager;
import uk.co.paulbenn.loadingcachemanager.provider.MyProvider;

import java.util.UUID;

@Service
public class MyService {

    private final LoadingCache<String, UUID> myCache;

    public MyService(LoadingCacheManager loadingCacheManager,
                     MyProvider myProvider) {
        this.myCache = loadingCacheManager.<String, UUID>newBuilder()
                .name("myCache")
                .loadFunction(myProvider::provide)
                .buildAndRegister();
    }

    public UUID get(String key) {
        return myCache.get(key);
    }
}
