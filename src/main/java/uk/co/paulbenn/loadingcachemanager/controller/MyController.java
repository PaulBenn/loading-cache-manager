package uk.co.paulbenn.loadingcachemanager.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.co.paulbenn.loadingcachemanager.service.MyService;

import java.util.UUID;

@RestController
@RequestMapping("proof-of-concept")
public class MyController {

    private final MyService myService;

    public MyController(MyService myService) {
        this.myService = myService;
    }

    @GetMapping("{key}")
    public UUID get(@PathVariable String key) {
        return myService.get(key);
    }
}
