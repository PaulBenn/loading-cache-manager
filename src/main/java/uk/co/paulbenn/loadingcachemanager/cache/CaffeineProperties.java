package uk.co.paulbenn.loadingcachemanager.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
@PropertySource("classpath:caffeine.properties")
@ConfigurationProperties("caffeine")
public class CaffeineProperties {

    @Setter
    private Map<String, String> spec;

    public Caffeine<Object, Object> getSpec(String name) {
        String specFromName = Optional.ofNullable(this.spec.get(name))
                .orElseThrow(
                        () -> new IllegalStateException(
                                "cache spec '" + name + "' is missing from caffeine.properties"
                        )
                );

        return Caffeine.from(specFromName);
    }
}
