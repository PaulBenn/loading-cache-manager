package uk.co.paulbenn.loadingcachemanager.cache;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class LoadingCacheManager {

    private final CaffeineProperties caffeineProperties;

    @SuppressWarnings("rawtypes")
    private final ConcurrentMap<String, LoadingCache> caches = new ConcurrentHashMap<>();

    public LoadingCacheManager(CaffeineProperties caffeineProperties) {
        this.caffeineProperties = caffeineProperties;
    }

    public <K, V> LoadingCacheBuilder<K, V> newBuilder() {
        return new LoadingCacheBuilder<>(this);
    }

    public <K, V> LoadingCache<K, V> getCache(String name) {
        @SuppressWarnings("unchecked")
        LoadingCache<K, V> cache = this.caches.get(name);

        return cache;
    }

    public Collection<String> getCacheNames() {
        return Collections.unmodifiableSet(this.caches.keySet());
    }

    private <K, V> void register(String name, LoadingCache<K, V> cache) {
        this.caches.putIfAbsent(name, cache);
    }

    public static class LoadingCacheBuilder<K, V> {

        private final LoadingCacheManager loadingCacheManager;

        private String name;
        private Caffeine<Object, Object> spec;
        private LoadFunction<K, V> loadFunction;
        private ReloadFunction<K, V> reloadFunction;

        public LoadingCacheBuilder(LoadingCacheManager loadingCacheManager) {
            this.loadingCacheManager = loadingCacheManager;
        }

        public LoadingCacheBuilder<K, V> name(String name) {
            this.name = name;
            return this;
        }

        public LoadingCacheBuilder<K, V> spec(String spec) {
            return spec(Caffeine.from(spec));
        }

        public LoadingCacheBuilder<K, V> spec(Caffeine<Object, Object> spec) {
            this.spec = spec;
            return this;
        }

        public LoadingCacheBuilder<K, V> loadFunction(LoadFunction<K, V> loadFunction) {
            this.loadFunction = loadFunction;
            return this;
        }

        public LoadingCacheBuilder<K, V> reloadFunction(ReloadFunction<K, V> reloadFunction) {
            this.reloadFunction = reloadFunction;
            return this;
        }

        public LoadingCache<K, V> buildAndRegister() {
            if (name == null) {
                throw new IllegalStateException("cannot construct cache without 'name'");
            }

            if (spec == null) {
                this.spec = loadingCacheManager.caffeineProperties.getSpec(name);
            }

            if (loadFunction == null) {
                throw new IllegalStateException("cannot construct cache without 'loadFunction'");
            }

            LoadingCache<K, V> cache;
            if (reloadFunction != null) {
                cache = build(spec, loadFunction, reloadFunction);
            } else {
                cache = build(spec, loadFunction);
            }

            loadingCacheManager.register(this.name, cache);

            return cache;
        }

        private LoadingCache<K, V> build(Caffeine<Object, Object> caffeine,
                                         LoadFunction<K, V> loadFunction) {
            return caffeine.build(new CacheLoader<>() {
                @Nullable
                @Override
                public V load(@NonNull K key) throws Exception {
                    return loadFunction.load(key);
                }

                @Override
                public V reload(@NonNull K key, @NonNull V oldValue) {
                    try {
                        return loadFunction.load(key);
                    } catch (Exception e) {
                        return oldValue;
                    }
                }
            });
        }

        public LoadingCache<K, V> build(Caffeine<Object, Object> caffeine,
                                        LoadFunction<K, V> loadFunction,
                                        ReloadFunction<K, V> reloadFunction) {
            return caffeine.build(new CacheLoader<>() {
                @Nullable
                @Override
                public V load(@NonNull K key) throws Exception {
                    return loadFunction.load(key);
                }

                @Override
                public V reload(@NonNull K key, @NonNull V oldValue) throws Exception {
                    return reloadFunction.reload(key, oldValue);
                }
            });
        }
    }
}
