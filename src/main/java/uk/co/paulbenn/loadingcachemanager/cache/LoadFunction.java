package uk.co.paulbenn.loadingcachemanager.cache;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

@FunctionalInterface
public interface LoadFunction<K, V> {
    @Nullable
    V load(@NonNull K key) throws Exception;
}
