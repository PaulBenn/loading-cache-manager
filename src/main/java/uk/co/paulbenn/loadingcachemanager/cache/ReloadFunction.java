package uk.co.paulbenn.loadingcachemanager.cache;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

@FunctionalInterface
public interface ReloadFunction<K, V> {
    @Nullable
    V reload(@NonNull K key, @NonNull V oldValue) throws Exception;
}
