package uk.co.paulbenn.loadingcachemanager.provider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class MyProvider {

    public UUID provide(String key) {
        return UUID.randomUUID();
    }
}
